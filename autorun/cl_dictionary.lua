DICT = {
	--["using Gmod;"] = "",
	-- in/out
	["Console.WriteLine("] = "print(" .. text .. "\n)",
	["Console.Write("] = "print(" .. text .. ")",
	-- functions
	["public static void Main() "] = "main() local function main()", -- + end
	["static int "] = "local function " .. name .. "()",
	["return "] = "return " .. vars,
	["."] = obj .. "." .. method .. "()",
	-- data types
	["char ", "bool ", "byte ", "sbyte ", "int ", "short ", "ushort ",
	 "long ", "ulong ", "uint ", "decimal ", "float ", "double ", "string "] = "local " .. name,
	["namespace "] = "NAMESPACE_" .. name .. " = {}",
	["class "] = "CLASS_" .. name .. " = {}",
	["struct "] = "STRUCT_" .. name .. " = {}",
	["enum "] = "ENUM_" .. name .. " = {}",
	-- conditions
	["if ("] = "if " .. cond .. operation .. cond2 .. " then", -- + end
	
}